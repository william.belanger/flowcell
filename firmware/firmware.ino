//#define HC05_INITIAL_SETUP
#define BUTTON 4
#define LED_CAL_GREEN 5
#define LED_CAL_RED 6
#define LED_SAT_GREEN 7
#define LED_SAT_RED 8
#define THERMOMETER 12
#define EMITTER A5
#define RECEIVER A7

#include <EEPROM.h>
#include <OneWire.h>
#include <SoftwareSerial.h>
#include <DallasTemperature.h>

OneWire oneWire(THERMOMETER);
DallasTemperature thermometers(&oneWire);
SoftwareSerial BtSerial(3, 2);  // MCU Rx, Tx

const unsigned int MAX_INPUT = 32;
unsigned int baseline;


unsigned int opt101_read()
{
  unsigned long val = 0;
  for (int i=0; i < 4096; i++)
    val = val + analogRead(RECEIVER);
  return (val/4096);
}


void opt101_zero()
{
  leds_off();
  digitalWrite(LED_CAL_GREEN, HIGH);
  baseline = opt101_read();
  EEPROM.put(0, baseline);
}


void leds_off()
{
  digitalWrite(LED_CAL_GREEN , LOW);
  digitalWrite(LED_CAL_RED , LOW);
  digitalWrite(LED_SAT_GREEN , LOW);
  digitalWrite(LED_SAT_RED , LOW);
}


void leds_on(unsigned int transmittance)
{
  leds_off();

  if (transmittance < 98)
    digitalWrite(LED_SAT_RED, HIGH);
  else
    digitalWrite(LED_SAT_GREEN, HIGH);

  if (transmittance > 101 || transmittance == 0)
    digitalWrite(LED_CAL_RED, HIGH);
}


void acquisition()
{
  thermometers.requestTemperatures();
  float temp_external = thermometers.getTempCByIndex(0);
  float temp_internal = thermometers.getTempCByIndex(1);

  float sig = opt101_read();
  float transmittance = (100*sig)/baseline;
  float absorbance = 2-log10(transmittance);
  leds_on(transmittance);

  BtSerial.print("signal="); BtSerial.print(sig, 0);
  BtSerial.print(";baseline="); BtSerial.print(baseline);
  BtSerial.print(";transmittance="); BtSerial.print(transmittance, 2);
  BtSerial.print(";absorbance="); BtSerial.print(absorbance, 4);
  BtSerial.print(";internal="); BtSerial.print(temp_internal, 2);
  BtSerial.print(";external="); BtSerial.println(temp_external, 2);

  //Serial.print("signal="); Serial.print(sig, 0);
  //Serial.print(";baseline="); Serial.print(baseline);
  //Serial.print(";transmittance="); Serial.print(transmittance, 2);
  //Serial.print(";absorbance="); Serial.print(absorbance, 4);
  //Serial.print(";internal="); Serial.print(temp_internal, 2);
  //Serial.print(";external="); Serial.println(temp_external, 2);
}


void cli_parse(const char* data)
{
  if (!strcmp(data, "zero"))
    opt101_zero();

  else if (!strcmp(data, "on"))
    digitalWrite(EMITTER, HIGH);

  else if (!strcmp(data, "off"))
    digitalWrite(EMITTER, LOW);

  else if (strcmp(data, "\n"))
    BtSerial.print("bad="); BtSerial.println(data);
}


void cli_read(const byte inByte)
{
  static char cli_line[MAX_INPUT];
  static unsigned int cli_pos = 0;
  switch (inByte)
  {
    case '\r':
      break;

    case '\n':
      cli_line[cli_pos] = 0;  // Terminating null byte
      cli_pos = 0;
      cli_parse(cli_line);
      break;

    default:
      if (cli_pos < (MAX_INPUT - 1))
        cli_line[cli_pos++] = inByte;
      break;
  }
}


#ifdef HC05_INITIAL_SETUP
int main(void)
{
  init();
  {
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
    Serial.begin(9600);
    BtSerial.begin(38400);
    Serial.println("Configuring HC-05 module in 10s ...");
    delay(10000);

    BtSerial.print("AT+ROLE=0\r\n"); delay(500);              // Set slave mode
    BtSerial.print("AT+UART=9600,0,0\r\n"); delay(500);       // Set baudrate
    BtSerial.print("AT+NAME=\"Flowcell\"\r\n"); delay(500);   // Set name
    BtSerial.print("AT+PSWD=\"1234\"\r\n"); delay(500);       // Set PIN code
    //BtSerial.print("AT+POLAR=1,0\r\n"); delay(500);         // Set LED IO for OTA upload
    digitalWrite(13, LOW);
  }

  while(1)
  {
    if (BtSerial.available())
      Serial.write(BtSerial.read());

    if (Serial.available())
      BtSerial.write(Serial.read());
  }
  return 0;
}

#else

int main(void)
{
  init();
  {
    pinMode(EMITTER, OUTPUT);
    pinMode(RECEIVER, INPUT);
    pinMode(BUTTON, INPUT_PULLUP);
    pinMode(LED_CAL_GREEN, OUTPUT);
    pinMode(LED_CAL_RED, OUTPUT);
    pinMode(LED_SAT_GREEN, OUTPUT);
    pinMode(LED_SAT_RED, OUTPUT);
    pinMode(THERMOMETER, INPUT);
    digitalWrite(EMITTER, HIGH);
    BtSerial.begin(9600);
    BtSerial.setTimeout(250);
    //Serial.begin(9600);

    thermometers.begin();
    thermometers.setResolution(10);
    thermometers.setWaitForConversion(false);

    EEPROM.get(0, baseline);
    if (baseline == 0)
      baseline = 1;
  }

  while(1)
  {
    if (digitalRead(BUTTON) == LOW)
      opt101_zero();

    while (BtSerial.available())
      cli_read(BtSerial.read());

    //while (Serial.available())
      //cli_read(Serial.read());

    acquisition();
  }
  return 0;
}
#endif
