#!/usr/bin/python3
import sys
from dataclasses import dataclass
from pathlib import Path
try:
    from ..__id__ import ID
except (ValueError, ImportError):
    from __id__ import ID


def configDir() -> Path:
    if sys.platform.startswith("win"):
        return Path.home() / ID
    return Path.home() / ".config" / ID


@dataclass
class Dirs:
    CFG = configDir()
    LOGS = CFG / "logs"
    GUI = Path(__file__).parents[1] / "frontend"


@dataclass
class Files:
    SETTINGS = Dirs.CFG / "settings.json"
    LOG = Dirs.LOGS / "session.log"
