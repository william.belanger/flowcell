#!/usr/bin/python3
from PyQt5 import QtCore, QtBluetooth
try:
    from .backend import logger
except (ValueError, ImportError):
    from backend import logger

log = logger.new(__name__)


class HC05(QtCore.QObject):
    continuous = QtCore.pyqtSignal(object)
    periodic = QtCore.pyqtSignal(object)

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.buffer = []
        self.last = ""

        protocol = QtBluetooth.QBluetoothServiceInfo.RfcommProtocol
        self.socket = QtBluetooth.QBluetoothSocket(protocol)
        self.socket.connected.connect(self.parent.connected)
        self.socket.disconnected.connect(self.parent.disconnected)
        self.socket.error.connect(self._error)

        self.agent = QtBluetooth.QBluetoothDeviceDiscoveryAgent(self)
        self.agent.setLowEnergyDiscoveryTimeout(2000)
        self.agent.deviceDiscovered.connect(self._add)
        self.agent.finished.connect(self._autoConnect)
        self.agent.error.connect(self._error)

        self.pullTimer = QtCore.QTimer(interval=100)
        self.pullTimer.timeout.connect(self.pull)
        self.pullTimer.start()

        self.pushTimer = QtCore.QTimer(interval=1000)
        self.pushTimer.timeout.connect(self.push)
        self.pushTimer.timeout.connect(self.resetPushTimer)

    @property
    def isConnected(self) -> bool:
        """ Return true if the device is connected """
        isConnected = bool(self.socket.state() == QtBluetooth.QBluetoothSocket.ConnectedState)
        return isConnected

    def connect(self):
        """ Initiate the connection """
        mac = self.parent.ui.macBox.currentText()
        active = self.parent.enabled and not self.isConnected
        if mac and active:
            mac = mac.split()[0]
            self.parent.db["mac"] = mac
            self.parent.setStatus(f"Connecting to {mac} ...")

            addr = QtBluetooth.QBluetoothAddress(mac)
            uuid = QtBluetooth.QBluetoothUuid(QtBluetooth.QBluetoothUuid.SerialPort)
            self.parent.connectTimer.stop()
            self.agent.stop()
            self.socket.abort()
            self.socket.connectToService(addr, uuid, QtCore.QIODevice.ReadWrite)

    def disconnect(self):
        """ Abort the connection """
        self.socket.abort()
        self.parent.setStatus("Disconnected")

    def pull(self):
        """ Read HC05 transmission and store it in the buffer """
        if self.isConnected:
            while self.socket.canReadLine():
                line = bytes(self.socket.readLine(1024))
                try:
                    line = line.decode("utf8").strip()
                    parsed = self._parse(line)
                    self.buffer.append(parsed)
                    self.continuous.emit(parsed)
                except UnicodeDecodeError:
                    self._resend()

    def push(self):
        """ Push latest buffer entry to console """
        if self.buffer:
            last = self.buffer[-1]
            self.periodic.emit(last)
            self.buffer.clear()

    def resetPushTimer(self):
        interval = self.parent.interval
        self.pushTimer.setInterval(interval)

    def send(self):
        """ Send a string to the Bluetooth device """
        cmd = self.parent.ui.sendLine.text()
        if cmd:
            data = f"{cmd}\n".encode("utf8")
            self.last = data
            self.socket.write(data)
            self.parent.ui.sendLine.clear()
            QtCore.QTimer.singleShot(250, self._newline)

    def start(self):
        self.pushTimer.start()

    def stop(self):
        self.pushTimer.stop()

    def _add(self, dev):
        """ Append the discovered device to the address combobox """
        name = dev.name()
        addr = dev.address().toString()
        if name != addr.replace(":", "-"):
            entry = f"{addr} ({name})"
            if self.parent.ui.macBox.findText(entry) == -1:
                self.parent.ui.macBox.addItem(entry)
                self._discovered(addr, name)

    def _autoConnect(self):
        """ Initiate a connection with the last used address on startup """
        last = self.parent.db["mac"]
        auto = self.parent.ui.autoconnectBox.isChecked()
        self.parent.setStatus("Waiting for connection")
        if last and auto:
            self.parent.ui.connectButton.setText("Disconnect")
            self.parent.ui.macBox.setEnabled(False)
            self.parent.enabled = True
            self.connect()
        else:
            devices = self.agent.discoveredDevices()
            hasFlowcell = [x for x in devices if self._isFlowcell(x.name())]
            if not hasFlowcell:
                error = "Flowcell device not found, please pair with the device and restart the application (PIN=1234)"
                self.parent.setStatus(error)

    def _discovered(self, addr, name):
        """ Restore last MAC address into the combobox, or find a new Flowcell device """
        mac = self.parent.db["mac"]
        isHC = not mac and name.startswith("Flowcell")
        if addr == mac or isHC:
            self.parent.ui.macBox.setCurrentText(f"{addr} ({name})")

    def _error(self, error):
        """ Send a disconnect signal and forward a connection error string to the main window """
        sender = QtCore.QObject.sender(self)
        error = sender.errorString().lower()
        self.parent.disconnected()
        self.parent.setStatus(f"Connection failed: {error}")

    def _isFlowcell(self, name):
        return bool(name.startswith("Flowcell") or name.startswith("HC-05"))

    def _newline(self):
        self.socket.write("\n".encode("utf8"))

    def _parse(self, line: str):
        """ Parse the raw data string into a dict and return it """
        data = {}
        try:
            for element in line.split(";"):
                key, value = element.split("=")
                if key in self.parent.headers:
                    data[key] = value
        except ValueError:
            pass
        return data

    def _resend(self):
        self.socket.write(self.last)
        QtCore.QTimer.singleShot(250, self._newline)
        log.warning(f"Resent {self.last}")
