#!/usr/bin/python3
import sys
from datetime import datetime
from pathlib import Path
from PyQt5 import QtWidgets, QtCore, QtGui, uic

try:
    from .__id__ import ID, APP_NAME
    from .backend import logger
    from .backend.hc05 import HC05
    from .backend.constants import Dirs, Files
    from .backend.database import Database
    from .frontend import flowcell as flowcell_ui
except (ValueError, ImportError):
    from __id__ import ID, APP_NAME
    from backend import logger
    from backend.hc05 import HC05
    from backend.constants import Dirs, Files
    from backend.database import Database

log = logger.new(__name__)


class Core(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.setup = Setup(self)
        self.timer = QtCore.QElapsedTimer()
        self.console.headers()
        self.show()

    @property
    def elapsed(self) -> str:
        delta = round(self.timer.elapsed(), -3)
        s = round(delta / 1000)
        h, s = divmod(s, 3600)
        m, s = divmod(s, 60)
        return f"{h:02d}:{m:02d}:{s:02d}"

    @property
    def headers(self):
        """ Return the list of columns used for the CSV columns """
        header = self.ui.headerLine.text().split(",")
        header.append("comment")
        return header

    @property
    def interval(self):
        """ Return the acquisition interval in milliseconds """
        interval = 1000 * self.ui.secondsBox.value()
        interval += 1000 * 60 * self.ui.minutesBox.value()
        interval += 1000 * 60 * 60 * self.ui.hoursBox.value()
        return interval

    @property
    def timestamp(self) -> dict:
        now = datetime.now()
        date = f"{now.day:02d}/{now.month:02d}/{now.year:02d}"
        clock = f"{now.hour:02d}:{now.minute:02d}:{now.second:02d}"
        return {"date": date, "time": clock, "elapsed": self.elapsed}

    def closeEvent(self, event=None):
        """ Save database before exit """
        self.db.save()
        self.parent.exit()

    def connected(self):
        """ Slot: connection successful """
        self.setStatus("Connected")
        self.ui.bufferLabel.setEnabled(True)
        self.ui.sendButton.setEnabled(True)
        self.sendLine.setEnabled(True)
        self.sendLine.setFocus(True)

        # Collect the announce data regardless of the set interval
        # Delayed read to skip incomplete packets
        self.bt.start()
        self.timer.restart()
        QtCore.QTimer.singleShot(1000, self.bt.push)

    def disconnected(self):
        """ Slot: connection terminated """
        self.bt.stop()
        self.ui.bufferLabel.setEnabled(False)
        self.ui.sendButton.setEnabled(False)
        self.sendLine.setEnabled(False)
        if self.enabled:
            self.connectTimer.start()

    def resetTimer(self):
        self.timerFlag = True

    def setStatus(self, status=""):
        """ Update main window status bar """
        self.statusBar.showMessage(status)

    def _render(self, data: dict):
        """ Append acquisition data to the console """
        if self.timerFlag:
            self.timerFlag = False
            self.timer.restart()
        data.update(self.timestamp)

        item = QtWidgets.QTreeWidgetItem()
        for h in self.headers:
            if h in data and h in self.headerTable:
                col = self.headerTable[h]
                item.setText(col, data[h])
                item.setTextAlignment(col, QtCore.Qt.AlignCenter)
        self.console.addTopLevelItem(item)
        self.console.setItemWidget(item, item.columnCount(), LineEdit())  # Editable comment column

        if self.ui.autoscrollBox.isChecked():
            self.console.scrollToItem(item, QtWidgets.QAbstractItemView.PositionAtCenter)

    def _reset(self):
        """ Reset configuration to default """
        self.db.reset()
        self.db.fields.load()

    def _status(self, data):
        """ Updates buffer label """
        status = self.ui.statusLine.text()  # %signal% / %baseline% (%internal% °C)
        for h in ("signal", "baseline", "transmittance", "absorbance",
                  "internal", "external", "date", "time", "elapsed"):
            status = status.replace(f"%{h}%", data.get(h, "-"))
        self.ui.bufferLabel.setText(status)

    def _toggle(self):
        """ Toggle the connection with the selected device """
        self.enabled = not self.enabled
        if self.enabled:
            self.ui.connectButton.setText("Disconnect")
            self.ui.macBox.setEnabled(False)
            self.bt.connect()
        else:
            self.ui.connectButton.setText("Connect")
            self.ui.macBox.setEnabled(True)
            self.bt.disconnect()


class Setup:
    """ Setups instances and variables for Core class """
    def __init__(self, parent):
        parent.enabled = False
        parent.timerFlag = False
        self.p = parent
        self.ui()
        self.settings()
        self.bluetooth()
        self.signals()

    def bluetooth(self):
        """ Initiate Bluetooth timers and populate the address combobox with paired devices """
        self.p.bt = HC05(self.p)
        self.p.connectTimer = QtCore.QTimer(interval=6000)
        self.p.connectTimer.timeout.connect(self.p.bt.connect)
        self.p.bt.continuous.connect(self.p._status)
        self.p.bt.periodic.connect(self.p._render)
        self.p.bt.agent.start()
        self.p.setStatus("Device discovery in progress ...")

    def ui(self):
        """ Load widgets and icons """
        try:
            self.p.ui = flowcell_ui.Ui_MainWindow()
            self.p.ui.setupUi(self.p)
        except NameError:
            path = Dirs.GUI / "flowcell.ui"
            self.p.ui = uic.loadUi(path, self.p)
        self.p.resize(1000, 400)

        path = Dirs.GUI / "flowcell.svg"
        icon = QtGui.QIcon(str(path))
        self.p.setWindowIcon(icon)
        self.p.setWindowTitle(APP_NAME)
        self.p.sendLine = SendLine(self.p)
        self.p.console = ConsoleTree(self.p)
        self.p.ui.sendLayout.insertWidget(0, self.p.sendLine)
        self.p.ui.consoleLayout.insertWidget(0, self.p.console)

        self.p.ui.bufferLabel = QtWidgets.QLabel()
        self.p.statusBar = self.p.statusBar()
        self.p.statusBar.addPermanentWidget(self.p.ui.bufferLabel)
        self.p.statusBar.setStyleSheet("QStatusBar::item { border: 0 }")

    def settings(self):
        """ Initiate the settings database and track widget values """
        self.p.db = Database(Files.SETTINGS)
        self.p.db.fields.track((
            self.p.ui.macBox,
            self.p.ui.autoconnectBox,
            self.p.ui.autoscrollBox,
            self.p.ui.hoursBox,
            self.p.ui.minutesBox,
            self.p.ui.secondsBox,
            self.p.ui.headerLine,
            self.p.ui.statusLine,
        ))

    def signals(self):
        self.p.ui.connectButton.clicked.connect(self.p._toggle)
        self.p.ui.resetButton.clicked.connect(self.p._reset)
        self.p.ui.clearButton.clicked.connect(self.p.console.clear)
        self.p.ui.saveButton.clicked.connect(self.p.console.save)
        self.p.ui.timerButton.clicked.connect(self.p.resetTimer)
        self.p.ui.headerLine.textChanged.connect(self.p.console.headers)

        self.p.ui.secondsBox.valueChanged.connect(self.p.bt.resetPushTimer)
        self.p.ui.minutesBox.valueChanged.connect(self.p.bt.resetPushTimer)
        self.p.ui.hoursBox.valueChanged.connect(self.p.bt.resetPushTimer)
        self.p.ui.sendButton.clicked.connect(self.p.bt.send)


class ConsoleTree(QtWidgets.QTreeWidget):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.setIndentation(0)
        self.header().setStretchLastSection(True)
        self.header().setDefaultAlignment(QtCore.Qt.AlignCenter)

    def clear(self):
        self.parent.resetTimer()
        super().clear()

    def headers(self):
        headers = self.parent.headers
        labels = tuple(map(str.capitalize, headers))
        cols = len(self.parent.headers)
        self.setHeaderLabels(labels)
        self.setColumnCount(cols)
        self.parent.headerTable = {}
        for index, h in enumerate(headers):
            self.parent.headerTable[h] = index
        self.clear()

    def save(self):
        """ Opens a save as prompt """
        now = datetime.now()
        timestamp = f"{now.year:02d}_{now.month:02d}_{now.day:02d}"
        dest = str(Path.home() / f"{timestamp}.csv")
        path = QtWidgets.QFileDialog.getSaveFileName(self, "Save view as", dest, "*.csv")[0]
        if path:
            self._save(path)

    def _save(self, path: str):
        """ Converts console tree content to CSV file """
        headers = self.parent.headers
        content = ";".join(headers) + "\n"
        for item in range(self.topLevelItemCount()):
            item = self.topLevelItem(item)
            line = [item.text(col) for col in range(len(headers) - 1)]
            line.append(self.itemWidget(item, item.columnCount()).text())  # Editable comment column
            content += ";".join(line) + "\n"
        try:
            with open(path, "w") as f:
                f.write(content.rstrip())
        except PermissionError:
            log.error(f"Could not write to file '{path}'")


class LineEdit(QtWidgets.QLineEdit):
    def __init__(self):
        super().__init__()
        self.setStyleSheet("border: none")


class SendLine(QtWidgets.QLineEdit):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.setEnabled(False)

    def keyPressEvent(self, event):
        if event.key() in (QtCore.Qt.Key_Enter, QtCore.Qt.Key_Return):
            self.parent.bt.send()
        QtWidgets.QLineEdit.keyPressEvent(self, event)


def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setDesktopFileName(ID)
    app.setApplicationName(APP_NAME)
    Core(app)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
