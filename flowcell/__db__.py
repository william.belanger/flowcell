#!/usr/bin/python3
DEFAULT = \
{
    "mac": "",
    "__tracked__":
    {
        "autoconnectBox": True,
        "autoscrollBox": True,
        "macBox": "",
        "hoursBox": 0,
        "minutesBox": 0,
        "secondsBox": 5,
        "headerLine": "signal,baseline,transmittance,absorbance,internal,external,date,time,elapsed",
        "statusLine": "%signal% / %baseline% (%internal% °C)",
    }
}
