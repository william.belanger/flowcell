#!/usr/bin/python3
import ctypes
import os
import subprocess
import sys

# Load __id__ from Pyinstaller bundled resources folder
sys.path.append(sys._MEIPASS)
from __id__ import ID


def install():
    for pkg in ("setuptools", "pywin32", ID):
        cmd = f"python -m pip install {pkg} --upgrade"
        subprocess.run(cmd.split(), stderr=sys.stderr, stdout=sys.stdout)
        print()


def main():
    shell = ctypes.windll.shell32
    if shell.IsUserAnAdmin():
        install()
        print()
    else:
        print("Error: This installer require admin privileges to run")
        print("Right click on the installer and select 'Run as administrator' to continue\n")
    os.system("pause")


if __name__ == '__main__':
    main()
