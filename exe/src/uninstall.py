#!/usr/bin/python3
import ctypes
import os
import subprocess
import sys

# Load __id__ from Pyinstaller bundled resources folder
sys.path.append(sys._MEIPASS)
from __id__ import ID, DEPENDANCIES


def remove_shortcut():
    try:
        from win32com.shell import shell, shellcon
        desktopFolder = shell.SHGetFolderPath(0, shellcon.CSIDL_DESKTOP, None, 0)
        shortcut = os.path.join(desktopFolder, f"{ID.title()}.lnk")
        os.remove(shortcut)
        print(f"Removed {ID} shortcut\n")
    except OSError:
        pass


def uninstall():
    packages = [ID] + DEPENDANCIES
    for pkg in packages:
        cmd = f"python -m pip uninstall -y {pkg}"
        subprocess.run(cmd.split(), stderr=sys.stderr, stdout=sys.stdout)
        print()


def main():
    shell = ctypes.windll.shell32
    if shell.IsUserAnAdmin():
        remove_shortcut()
        uninstall()
        print()
    else:
        print("Error: This uninstaller require admin privileges to run")
        print("Right click on the installer and select 'Run as administrator' to continue\n")
    os.system("pause")


if __name__ == '__main__':
    main()
