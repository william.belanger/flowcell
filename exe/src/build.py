#!/usr/bin/python3
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile
import PyInstaller.__main__

tmp_dir = tempfile.gettempdir()
build_dir = os.path.join(tmp_dir, "build")

local_dir = str(pathlib.Path(__file__).parent)
parent_dir = str(pathlib.Path(__file__).parents[1])
id_file = str(pathlib.Path(__file__).parents[2].joinpath("__id__.py"))


def main():
    cmd = f"python -m pip install pywin32 --upgrade"
    subprocess.run(cmd.split(), stderr=sys.stderr, stdout=sys.stdout)

    for name in ("install", "uninstall"):
        icon = os.path.join(local_dir, f"{name}.ico")
        PyInstaller.__main__.run([
            "--onefile",
            "--name=%s" % name,
            "--icon=%s" % icon,
            "--distpath=%s" % parent_dir,
            "--workpath=%s" % build_dir,
            "--specpath=%s" % tmp_dir,
            "--add-data=%s;." % id_file,
            os.path.join(local_dir, f"{name}.py"),
        ])
    shutil.rmtree(os.path.join(local_dir, "__pycache__"))
    os.system("pause")


if __name__ == '__main__':
    main()
