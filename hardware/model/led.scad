include <lib/isothread.scad>

bolt_blank = false;  // Print a blank bolt, without LED hole
led_h = 9.9;
led_h1 = 2.75;
led_r1 = 5.8 / 2;
led_r2 = 5.15 / 2;
led_opercule = 4 / 2;
led_leads_thickness = 0;
dallas_base = 1.2;

bolt_m = 8;  // Size [1.4,1.6,2,2.5,3,4,5,6,7,8,10,12,14,16,18,20,22,24,27,30,33,36]
bolt_l = bolt_m + 2.40;  // 6mm thread length, including bolt head
nut_h = 6;  // Thread length
nut_t = 0.2;  // Clearance for printer tolerance for nut


module led_assembly()
{
  rotate([180, 0, 0])
    difference() {
      iso_bolt(m=bolt_m, l=bolt_l);
      if (!bolt_blank) {
        cylinder(h=led_h, r=led_r2);
        cylinder(h=led_h1, r=led_r1);
        cylinder(h=cell_z+1, r=led_opercule);
      }
    }
}


module led_extension()
{
  color([0,1,0])
    difference() {
      cylinder(h=led_h1 + 1.5, r=bolt_m - 1.6);
      cylinder(h=led_h1 + 1.5, r=led_r1);
    }
}


module dallas_probe()
{
  clearance = 0.65;
  h = 4.34 + 1.5 + clearance;
  d = 4.38 + clearance;
  r = d / 2;
  difference()
  {
    cylinder(h = h, r = r);
    translate(v = [0, -r, 0])
      cube(size = [r, d, d]);
  }
  translate([0, -r, 0])
    cube(size = [dallas_base, d, h]);
}


module led_assembly_dallas()
{
  difference()
  {
    union() {
      led_assembly();
      led_extension();
    }
    color([1,0,0])
    translate([-dallas_base - led_leads_thickness, 0, -2.12])
      dallas_probe();
  }
}
