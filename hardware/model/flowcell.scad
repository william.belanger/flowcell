include <lib/shape.scad>
include <lib/isothread.scad>
include <led.scad>

cell_n = 1;  // Number of source/sensor pair
cell_x = 30;
cell_y = 30;
cell_z = 20;

pcb_x = 17;
pcb_y = 25;
pcb_z = 4;

opt_x = 11;
opt_y = 9.2;
opt_z = 7.3;
opt_top = 8.05;
opt_bottom = 7.65;

opercule_r = 0.5;
pipette_r = 3.57;  // Snug fit for regular size, ~7 mm diameter
threaded_inserts_depth = 5.7;
threaded_inserts_diameter = 4.5;


module sensor()
{
  // Internal hole (OPT101)
  translate([opt_top-opt_bottom, -cell_y/2, 0])
    rotate([90, 90, 0])
    cube(size=[opt_x, opt_y, opt_z+pcb_z], center=true);

  // Internal hole (pins headers)
  translate([-opt_x, -cell_y/2, 0])
    rotate([90, 90, 0])
    cube(size=[pcb_x, 3.55, opt_z/2+pcb_z], center=true);

  // Internal hole (screw holes)
  translate([opt_x, -cell_y/2, 0])
    rotate([90, 90, 0])
    cube(size=[pcb_x, 5, opt_z/2+pcb_z], center=true);

  // External hole (CJMCU-101)
  translate([0, -cell_y/2, 0])
    rotate([90, 90, 0])
    cube(size=[pcb_x, pcb_y, pcb_z], center=true);
}

module pipette()
{
  translate([opt_top - opt_bottom, 0, 0])
    cylinder(h=cell_z, r=pipette_r, center=true);
}

module opercules()
{
  translate([opt_top - opt_bottom, 0, 0])
    rotate([90, 0, 0])
    cylinder(h=cell_y, r=opercule_r, center=true);
}

module body()
{
  difference() {
    cube_rounded(cell_x, cell_y, cell_z, r=1.5, center=true);
    pipette();
    sensor();
    opercules();
  }
}

module threaded_inserts_pockets()
{
  depth = threaded_inserts_depth + 2;
  diameter = threaded_inserts_diameter + 4.5;

  translate ([0, cell_y/5, 0])
    rotate([90, 0, 90])
    translate([0, 0, cell_z - depth])
    cylinder(h=depth, r=1, center=true);

  translate ([0, -cell_y/5, 0])
    rotate([90, 0, 90])
    translate([0, 0, cell_z - depth])
    cylinder(h=depth, r=1, center=true);
}

module cell()
{
  difference() {
    body();
    translate([opt_top - opt_bottom, cell_y/2, 0])
      rotate([90, 0, 0])
      cylinder(r=bolt_m - 3, h=nut_h);
  }

  translate([opt_top - opt_bottom, cell_y/2, 0])
    rotate([90, 0, 0])
    iso_nut(w=nut_h, m=bolt_m, t=nut_t);
}

module main()
{
  for (i=[0:cell_n-1])
    translate([0, 0, cell_z*i])
      difference() {
        cell();
        threaded_inserts_pockets();
      }
}
