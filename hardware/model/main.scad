include <flowcell.scad>

module test_pipette()
{
  difference() {
    cube_rounded(pipette_r + 10, pipette_r + 10, 5, r=1.5, center=true);
    pipette();
  }
}

module led_demo()
{
  translate([opt_top - opt_bottom, 45,0])
    rotate([270,0,0])
    led_assembly();
}


$fn = 100;
cell_n = 1;  // Number of source/sensor pair
pipette_r = 3.57;  // Snug fit for regular size, ~7 mm diameter
/*bolt_blank = true;  // Print a blank bolt (placeholder) */

main();
/*test_pipette();*/
/*led_demo();*/
/*led_assembly();*/
/*led_assembly_dallas();*/
