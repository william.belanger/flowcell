module cube_rounded(x, y, z, r, center) {
  hull() {
    translate([-x/2+r, -y/2+r, 0])
      cylinder(r=r, h=z, center=center);
    translate([x/2-r, y/2-r, 0])
      cylinder(r=r, h=z, center=center);
    translate([-x/2+r, y/2-r, 0])
      cylinder(r=r, h=z, center=center);
    translate([x/2-r, -y/2+r, 0])
      cylinder(r=r, h=z, center=center);
  }
}
