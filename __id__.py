VERSION = "0.0.6"

ID = "flowcell"
APP_NAME = "Flowcell"
DEPENDANCIES = ["pyqt5"]
DESCRIPTION = "Acquisition software for wireless flow-through spectrophotometer"
KEYWORDS = "spectrometry photometer spectrophotometer uv-vis hc05 opt101 biotechnology"
AUTHOR = "William Belanger"
URL = f"https://gitlab.com/william.belanger/{ID}"

CLASSIFIERS = [
"License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
"Programming Language :: Python :: 3.6",
"Operating System :: POSIX :: Linux",
"Operating System :: Microsoft :: Windows :: Windows 10",
]

SHORTCUTS = [
    ("", "Flowcell"),
]
