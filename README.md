# Introduction
...

# Images
<!--**Assembled device***-->

<!--![alt tag](https://gitlab.com/william.belanger/flowcell/raw/master/extra/device.gif)-->

**User interface**

![alt tag](https://gitlab.com/william.belanger/flowcell/raw/master/extra/ui_demo.gif)

# Schematic
**Logic board**

![alt tag](https://gitlab.com/william.belanger/flowcell/raw/master/hardware/schematic/board.png)

**Peripherals**

![alt tag](https://gitlab.com/william.belanger/flowcell/raw/master/hardware/schematic/peripherals.png)

# Setup
**Hardware:**
- Print the spectrophotometer [cell body](https://gitlab.com/william.belanger/flowcell/-/blob/master/hardware/cell_body.stl)
- If the cell printed by fused deposition modeling (FDM), clean the center of with a drill press and a 7mm drill bit
- Solder a capacitor and a resistor onto the OPT101 breakout board
- Clean the sensor with isopropanol and a non-abrasive, lint-free tissue
- Secure the LED, the sensor board, and cover all interferents light sources with black hot glue
- Reproduce the proposed schematic on a PCB or perfboard
- Cut and melt (soften) the tip of a Pasteur pipette, clean with isopropanol and insert into the cell
- Assemble everything and secure in an opaque box

**Firmware:**
- Uncomment the first line of the [firmware](https://gitlab.com/william.belanger/flowcell/-/blob/master/firmware/firmware.ino) file
- Upload the modified firmware to the MCU, disconnect from USB
- While holding the HC05 'State' button, power the circuit. Wait until HC05 LED blink slowly
- Wait until the MCU onboard LED turn off
- Upload the unmodified firmware, turn the power off and on again

**Client computer:**
- Start a Bluetooth device discovery and pair with 'Flowcell'
- Enter the PIN code (1234)
- Install and launch the software

# Acquisition software installation
**Windows 10:**
- Install the latest version of [Python](https://www.python.org/downloads/) (3.6 and above)
- On the first page of the installer, check **Add Python to PATH**
- Click on **Install Now**. If you choose *Customize installation*, leave *pip* checked
- Download the [install script for Windows](https://gitlab.com/william.belanger/flowcell/raw/master/exe/install.exe?inline=false)
- Right click on the installer and select "Run as Administrator"

**Linux (Debian):**
```
sudo apt-get update
sudo apt-get install python3 python3-pip python3-setuptools
sudo pip3 install --upgrade flowcell
```

**Linux (Arch):**
```
sudo pacman -Syu python python-pip python-setuptools
sudo pip install --upgrade flowcell
```

# Acknowledgement
- Original probe idea by [Alexander Kutschera](https://github.com/vektorious/test_tube_photometer) (License CC-BY-SA-4.0)
- Application icon modified from the [Numix Project](https://github.com/numixproject/numix-icon-theme) (License GPL-3.0)
